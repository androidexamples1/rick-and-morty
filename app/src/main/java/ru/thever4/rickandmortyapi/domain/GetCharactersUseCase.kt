package ru.thever4.rickandmortyapi.domain

import ru.thever4.rickandmortyapi.data.model.Character

interface GetCharactersUseCase {

    suspend operator fun invoke(): Result<List<Character>>

}

// TODO Create GetCharacterUseCaseImpl implementation here