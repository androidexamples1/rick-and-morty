package ru.thever4.rickandmortyapi.presenter.mainScreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.thever4.rickandmortyapi.domain.GetCharactersUseCase
import ru.thever4.rickandmortyapi.presenter.UiState
import ru.thever4.rickandmortyapi.data.model.Character
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getCharactersUseCase: GetCharactersUseCase, // TODO Don't forget to bind implementation
) : ViewModel() {

    private val _characters = MutableLiveData<UiState<List<Character>>>(UiState.Loading)
    val characters: LiveData<UiState<List<Character>>>
        get() = _characters

    init {
        viewModelScope.launch {
            // TODO call getCharactersUseCase and postValue to the _characters livedata
        }
    }


}