package ru.thever4.rickandmortyapi.presenter.mainScreen

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.thever4.rickandmortyapi.data.model.Character
import ru.thever4.rickandmortyapi.databinding.ItemCharacterBinding

class CharactersAdapter: ListAdapter<Character, CharactersAdapter.CharacterViewHolder>(CharacterDiffUtil()) {

    // TODO Implement missing members for adapter

    class CharacterDiffUtil: DiffUtil.ItemCallback<Character>() {
        // TODO Implement missing members for diffutil
    }

    class CharacterViewHolder(
        private val binding: ItemCharacterBinding,
    ): RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Character) {
            TODO("Implement bind method for CharacterViewHolder")
        }

    }

}