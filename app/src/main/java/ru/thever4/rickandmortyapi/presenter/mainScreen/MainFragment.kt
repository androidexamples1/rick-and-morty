package ru.thever4.rickandmortyapi.presenter.mainScreen

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import ru.thever4.rickandmortyapi.R
import ru.thever4.rickandmortyapi.databinding.FragmentMainBinding
import ru.thever4.rickandmortyapi.di.ViewModelFactory
import ru.thever4.rickandmortyapi.di.appComponent
import ru.thever4.rickandmortyapi.presenter.UiState
import javax.inject.Inject

class MainFragment : Fragment(R.layout.fragment_main) {

    private val binding: FragmentMainBinding by viewBinding()

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel: MainViewModel by viewModels { viewModelFactory }

    // TODO Create your adapter
    private val adapter: CharactersAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        initObservers()
        initRecycler()

        super.onViewCreated(view, savedInstanceState)
    }


    private fun initRecycler() {
        // TODO init adapter, set layoutmanager (grid/linear), set binding.recycler.adapter
    }


    private fun initObservers() {
        // TODO observe viewmodel livedata, submitList to adapter
    }

    override fun onAttach(context: Context) {
        context.appComponent.inject(this)
        super.onAttach(context)
    }

}