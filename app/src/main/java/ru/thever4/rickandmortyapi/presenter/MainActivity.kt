package ru.thever4.rickandmortyapi.presenter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.thever4.rickandmortyapi.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}