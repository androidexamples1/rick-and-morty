package ru.thever4.rickandmortyapi.di

import dagger.Component
import dagger.Module
import ru.thever4.rickandmortyapi.presenter.mainScreen.MainFragment

@Component(
    modules = [AppModule::class]
)
interface AppComponent {
    fun inject(fragment: MainFragment)
}

@Module(includes = [
    AppBindsModule::class,
    ViewModelModule::class,
])
interface AppModule