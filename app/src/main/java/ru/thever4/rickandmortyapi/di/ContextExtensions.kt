package ru.thever4.rickandmortyapi.di

import android.content.Context
import ru.thever4.rickandmortyapi.MyApplication

val Context.appComponent: AppComponent
    get() = when(this) {
        is MyApplication -> appComponent
        else -> applicationContext.appComponent
    }