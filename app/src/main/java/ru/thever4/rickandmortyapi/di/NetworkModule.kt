package ru.thever4.rickandmortyapi.di

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import ru.thever4.rickandmortyapi.data.service.RickAndMortyService

@Module
class NetworkModule {

    @Provides
    fun provideRickAndMortyService(): RickAndMortyService =
        Retrofit.Builder()
            .baseUrl("https://rickandmortyapi.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()

}