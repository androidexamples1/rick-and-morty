package ru.thever4.rickandmortyapi.data.service

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import ru.thever4.rickandmortyapi.data.model.dto.CharactersInfoDto

interface RickAndMortyService {

    @GET("character")
    fun getCharacters(@Query("page") page: Int): Response<CharactersInfoDto>

}