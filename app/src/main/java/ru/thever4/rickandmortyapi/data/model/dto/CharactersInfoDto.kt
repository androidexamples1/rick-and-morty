package ru.thever4.rickandmortyapi.data.model.dto

import com.google.gson.annotations.SerializedName
import ru.thever4.rickandmortyapi.data.model.Character

data class CharactersInfoDto(
    @SerializedName("info")
    val pageInfo: PageInfoDto,
    @SerializedName("results")
    val characters: List<CharacterDto>
)

data class CharacterDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("species")
    val species: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("image")
    val image: String,
) {
    fun toCharacter(): Character =
        Character(name) // TODO Don't forget to add here missing values

    companion object {
        @JvmStatic
        fun toCharacter(dto: CharacterDto): Character =
            dto.toCharacter()
    }
}

data class PageInfoDto(
    @SerializedName("count")
    val count: Int,
    @SerializedName("pages")
    val pages: Int,
    @SerializedName("next")
    val next: String?,
    @SerializedName("prev")
    val prev: String?,
)