package ru.thever4.rickandmortyapi

import android.app.Application
import ru.thever4.rickandmortyapi.di.AppComponent
import ru.thever4.rickandmortyapi.di.DaggerAppComponent

class MyApplication: Application() {

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        appComponent = DaggerAppComponent.create()

        super.onCreate()
    }

}